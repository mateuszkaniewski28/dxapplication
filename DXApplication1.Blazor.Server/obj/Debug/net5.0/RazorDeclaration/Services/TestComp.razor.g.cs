// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace DXApplication1.Blazor.Server.Services
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Dev\source\repos\DXApplication1\DXApplication1.Blazor.Server\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Dev\source\repos\DXApplication1\DXApplication1.Blazor.Server\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Dev\source\repos\DXApplication1\DXApplication1.Blazor.Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Dev\source\repos\DXApplication1\DXApplication1.Blazor.Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Dev\source\repos\DXApplication1\DXApplication1.Blazor.Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Dev\source\repos\DXApplication1\DXApplication1.Blazor.Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Dev\source\repos\DXApplication1\DXApplication1.Blazor.Server\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Dev\source\repos\DXApplication1\DXApplication1.Blazor.Server\_Imports.razor"
using DevExpress.ExpressApp.Blazor.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Dev\source\repos\DXApplication1\DXApplication1.Blazor.Server\_Imports.razor"
using DXApplication1.Blazor.Server;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Users\Dev\source\repos\DXApplication1\DXApplication1.Blazor.Server\Services\TestComp.razor"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Dev\source\repos\DXApplication1\DXApplication1.Blazor.Server\Services\TestComp.razor"
using System.Text.Json.Serialization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Dev\source\repos\DXApplication1\DXApplication1.Blazor.Server\Services\TestComp.razor"
using DevExpress.Blazor;

#line default
#line hidden
#nullable disable
    public partial class TestComp : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 20 "C:\Users\Dev\source\repos\DXApplication1\DXApplication1.Blazor.Server\Services\TestComp.razor"
       
    private IEnumerable<GitHubBranch> branches = Array.Empty<GitHubBranch>();
    private bool getBranchesError;
    private bool shouldRender;

    protected override bool ShouldRender() => shouldRender;

    protected async Task GetBranches()
    {
        var request = new HttpRequestMessage(HttpMethod.Get,
            "https://api.github.com/repos/dotnet/AspNetCore.Docs/branches");
        request.Headers.Add("Accept", "application/vnd.github.v3+json");
        request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

        var client = ClientFactory.CreateClient();

        var response = await client.SendAsync(request);

        if (response.IsSuccessStatusCode)
        {
            using var responseStream = await response.Content.ReadAsStreamAsync();
            branches = await JsonSerializer.DeserializeAsync
                <IEnumerable<GitHubBranch>>(responseStream);
        }
        else
        {
            getBranchesError = true;
        }

        shouldRender = true;
    }

    public class GitHubBranch
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }
    }

    public class SingleNIPModel
    {

    }


#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IHttpClientFactory ClientFactory { get; set; }
    }
}
#pragma warning restore 1591
